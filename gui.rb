#! ruby -Ks
# -*- coding: utf-8 -*-
# ファイル名 gui.rb
STDOUT.sync = true 
require"./ranking.rb"
require"./cleanGameOdds.rb"
require("tk")

#あまりくん作成のクラスRankingを継承
class Rank < Ranking
  def return_name()
    return @names
  end
end

#神津くん作成のクラスCleanActionを継承
class Caction < CleanAction
  def return_task()
    return @actionName
  end
end


class Gui
  def initialize(rank,action,name_arr,task_arr)
    @rank = rank
    @cact = action
    @name_list = Array.new
    @name_list = name_arr
    @task_list = Array.new
    @task_list = task_arr
    @txt01 = "ランキング表示"
    @txt02 = "オッズ更新"
    @txt99 = "終了"
  end

  def gui()
#---------------------------------変数初期化------------------------------
    list = @name_list
    tlist = @task_list 
    r = @rank
    ca = @cact
    #ボタンテキスト
    t01 = @txt01
    t02 = @txt02
    t99 = @txt99
    player = "xxxx"
#--------------------------------------------------------------------------

    TkRoot.new do   #ウインドウのタイトルバー表示
      title( "掃除Game" )
    end
    
    TkButton.new do #ランキングの表示ボタン
      text( t01 )
      command do
        r.print_ranking()
        puts("---------------------------------------")
      end
      pack('side'=>'left','fill'=>'both','expand'=>'true')
    end
    
    TkButton.new do #オッズの更新、表示ボタン
      text( t02 )
      command do
        ca.odds()
        puts("---------------------------------------")
      end
      pack('side'=>'left','fill'=>'both','expand'=>'true')
    end
    
#---------------------------------------掃除コマンド------------------------------------
    TkLabel.new do     #ラベル表示
      #width  (30) #TkLabelの表示の大きさ　幅
      #height (10) #TkLabelの表示の大きさ　高さ
      text( "掃除コマンド" )
      background('red') #壁紙白色指定
      pack('side'=> 'top','fill'=>'both','expand'=>'true')
    end

    tlist.each do |task|
      TkButton.new do         #ボタン表示
        text( task )
        command do
          puts(player + "が" + task + "を実行")
          puts(ca.done(task))
          puts()
        end
        pack('side'=>'top','fill'=>'both','expand'=>'true')
      end
    end
#---------------------------------------プレイヤ選択-------------------------------------
    TkLabel.new do          #ラベル表示
      #width  (30) #TkLabelの表示の大きさ　幅
      #height (10) #TkLabelの表示の大きさ　高さ
      text( "プレイヤ選択" )
      background('green') #壁紙白色指定
      pack('side'=> 'top','fill'=>'both','expand'=>'true')
    end

    list.each do |name|
      TkButton.new do         #ボタン表示
        text( name )
        command do
          puts("現在のプレイヤ  " + name)
          puts()
          player = name
        end
        pack('side'=>'top','fill'=>'both','expand'=>'true')
      end
    end
 #--------------------------------------------------------------------------------------
    TkButton.new do              #終了ボタン表示
      text( t99 )
      command do #ボタンを押したときの動作を指示
        proc ( exit )
      end
      pack('side' => 'top' ,'fill'=>'both','expand'=>'true')#配置
    end
    
    Tk.mainloop
  end #gui()_end

end #class_end


def main

  arr = Array.new
  task = Array.new
  rank = Rank.new()
  action = Caction.new()
  arr = rank.return_name()
  task = action.return_task()
  odd = 1

  g1 = Gui.new(rank,action,arr,task);
  g1.gui();

end

main
