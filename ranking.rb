class Ranking
  def initialize()
    @names = []
    @scores = []
    space = 0
    s = ""
    File.open('ranking.txt') do |file|
      file.each_char do |char|
        if char == " " then
          @names.push(s)
          s = ""
          space = space + 1
        elsif char == "\n" then
          s = s.to_i
          @scores.push(s)
          s = ""
        else
          s.concat(char)
        end
      end
    end
  end

  def sort_rank()
    copy_scores = []
    copy_names = []
    num = @names.length
    i = 0
    while i < num do
      copy_scores[i] = @scores[i]
      copy_names[i] = @names[i]
      i = i + 1
    end
    copy_scores.sort!
    copy_scores.reverse!
    i = 0
    while i < num do
      j = 0
      while j < num do
        if copy_scores[i] == @scores[j] then
          @names[i] = copy_names[j]
          while copy_scores[i+1] == copy_scores[i] do
            @names[i+1] = copy_names[j+1]
            i = i + 1
            j = j + 1
          end
        end
        j = j + 1
      end
      i = i + 1
    end
    @scores = copy_scores
  end 

  def add_score(name, point)
    num_names = @names.length
    num = 0
    while num < num_names do
      if @names[num] == name then
        @scores[num] = @scores[num] + point
      end
      num = num + 1
    end
    sort_rank()
  end

  def print_ranking()
    num = @names.length
    i = 1
    File.open("ranking.txt", "w") do |file|
      while i <= num do
        printf("%d. %s %s\n", i, @names[i-1], @scores[i-1])
        file.printf("%s %s\n", @names[i-1], @scores[i-1])
        i = i + 1
      end
    end
  end

  def print_name(num)
    puts(@names[num])
  end

  def print_score(num)
    puts(@scores[num])
  end
end
